﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisualCheckService.Configuration;
using VisualCheckService.Interfaces;
using VisualCheckService.Services;

namespace VisualCheckService.Factories
{
    public static class ServiceFactory
    {
        public static ICleanUpService CreateService(CleanUpServiceType serviceType, EventLog eventLog)
        {
            switch (serviceType)
            {
                case CleanUpServiceType.File:
                    return new FileCleanUpService(eventLog);
                default:
                    return null;
            }
        }
    }
}
