# VisualCheckService VS2010 version #

VisualCheckService is a windows service that will run at log in or start up and deletes all media files that match a specific pattern and file type, from a specified directory. The service will write success and error messages to Event Viewer.

The service's configurable settings are defined in the App.Config and are as as follows:

* Directory - This is the directory to search for media files to remove.

* Pattern - This is the string pattern to match with file names, currently set to '*_?_????????_??????.???' - this is not a regular expression but rather a format that System.IO recognises.

* SupportedFileTypes - A CSV list representing file types that are supported, currently set to '.mp4,.jpg'

* ServiceType - The service is written in such a way that it could be extended to support other cleanUp service, e.g. a database service, or http service. It is currently only implemented with 'File'
* EventLogSource - Setting to define how logging messages show in event viewer, set to 'VisualCheckService'
* EventLog - Setting to indicate what event log messages will post to, currently set to 'Application'

### How do I get set up? ###

* Download repository
* Open in Visual Studio
* Build the project
* Open a command prompt - as Administrator - and got to /bin/debug in project directory
* Run the following command:* installutil.exe VisualCheckService.exe*
* The following link gives further info: http://msdn.microsoft.com/en-us/library/sd8zc8ha(v=vs.110).aspx